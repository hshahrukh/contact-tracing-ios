//
//  iBeaconConfig.swift
//  Contact Tracing
//
//  Created by ShahRukh on 02/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import Foundation


class iBeaconConfig
{
    // You can use uuidgen in terminal to generater new one.
    static let uuid = UUID(uuidString: "7FA08BC7-A55F-45FC-85C0-0BF26F899530")! //This is random uuid .it'll get initialize from uuid given by server
    
    private init() {}
}
