//
//  TraceConstants.swift
//  Contact Tracing
//
//  Created by ShahRukh on 03/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import Foundation


let kTraceDataFolder = "traceData"
let kMajor_minor_hexLen = 4
let kbinPathKey = "binPathKey"
let kNoBinFile = "There is no data available to upload"

let kTotalNumSecInDay = 60*60*1
let kTotalNumSecInMin = 60
let kTotalNumSecInMonth = 86400*31

//struct BinaryFiles: Codable {
//    
//    var name: String?
//    var path: URL?
//    
//    init(name:String , imageData: Data) {
//        self.name = name
//        self.path = URL
//    }
//    func toDict()-> Dictionary<String, Data> {
//        
//        
//        do {
//            var  dic = [String:Data]()
//             dic[name!] = path!
//            return dic
//        }
//        catch{
//            print("error")
//        }
//        
//    }
//    
//}
