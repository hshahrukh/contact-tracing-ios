//
//  DBHelper.swift
//  Contact Tracing
//
//  Created by ShahRukh on 03/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import Foundation
import SQLite3

class DBHelper
{
    init()
    {
        db = openDatabase()
        createTable()
    }

    let dbPath: String = "traceDB.sqlite"
    var db:OpaquePointer?

    func openDatabase() -> OpaquePointer?
    {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(dbPath)
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK
        {
            print("error opening database")
            return nil
        }
        else
        {
            print("Successfully opened connection to database at \(dbPath)")
            return db
        }
    }
    
    func createTable() {
        let createTableString = "CREATE TABLE IF NOT EXISTS traces (uuid TEXT,timestamp TEXT,other TEXT);"
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK
        {
            if sqlite3_step(createTableStatement) == SQLITE_DONE
            {
                print("traces table created.")
            } else {
                print("traces table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    
    func insert(uuid:String, timestamp:String, other:String)
    {
//        let traces = read()
//        for p in traces
//        {
//            if p.uuid == id
//            {
//                return
//            }
//        }
        let insertStatementString = "INSERT INTO traces (uuid, timestamp, other) VALUES (?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (uuid as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 2, (timestamp as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 3, (other as NSString).utf8String, -1, nil)
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    func read() -> [Traces] {
        let queryStatementString = "SELECT * FROM traces;"
        var queryStatement: OpaquePointer? = nil
        var trace : [Traces] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let uuid = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let timestamp = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let other = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                trace.append(Traces(uuid: uuid, timestamp: timestamp, other: other))
                print("Query Result:")
                print("\(uuid) | \(timestamp) | \(other)")
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return trace
    }
    
    
func readdByID(uuid:String)-> [Traces]  {
            
            let queryStatementString = "SELECT * FROM traces WHERE uuid = ?;"
             var queryStatement: OpaquePointer? = nil
             var trace : [Traces] = []
             if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                sqlite3_bind_text(queryStatement, 1,uuid, -1, nil)
                while sqlite3_step(queryStatement) == SQLITE_ROW {
                     let uuid = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                     let timestamp = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                     let other = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                     trace.append(Traces(uuid: uuid, timestamp: timestamp, other: other))
                     print("Query Result:")
                     print("\(uuid) | \(timestamp) | \(other)")
                 }
             } else {
                 print("SELECT statement could not be prepared")
             }
             sqlite3_finalize(queryStatement)
             return trace
            
            
}
    
    
    
    func deleteByID(uuid:String) {
        let deleteStatementStirng = "DELETE FROM traces WHERE uuid = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(deleteStatement, 1,uuid, -1, nil)
//            sqlite3_bind_int(deleteStatement, 1, uuid)
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
    }
    
    
    func deleteAllRows() {
            let deleteStatementStirng = "DELETE FROM traces"
            var deleteStatement: OpaquePointer? = nil
            if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
                if sqlite3_step(deleteStatement) == SQLITE_DONE {
                    print("Successfully deleted all rows.")
                } else {
                    print("Could not delete row.")
                }
            } else {
                print("DELETE statement could not be prepared")
            }
            sqlite3_finalize(deleteStatement)
    }
    
    
}
