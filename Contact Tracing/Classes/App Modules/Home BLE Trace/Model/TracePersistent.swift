//
//  TracePersistent.swift
//  Contact Tracing
//
//  Created by ShahRukh on 04/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import Foundation

class  TracePersistent: NSObject {

func saveBinFilePath(dataDict: Dictionary<String, String>) {
 
    var storingData : Dictionary<String,Any?>
    storingData = dataDict
    let userDefaults: UserDefaults = UserDefaults.standard
    let storedJson = userDefaults.value(forKeyPath: kbinPathKey) as? String
    if storedJson != nil {
    let storedData = GlobalFN().jsonToDict(jsonString: storedJson!) as!  Dictionary<String, AnyObject?>
    for (key, value) in storedData {
        if storingData[key] == nil {
        storingData[key] = value as? Any
        }
    }
    }
    let jsonString = GlobalFN().dictToJSON(dict: storingData as NSDictionary)
    userDefaults.set(jsonString, forKey: kbinPathKey)
    userDefaults.synchronize()
}


func getBinFilePath() ->Dictionary<String, String> {
    
    let userDefaults: UserDefaults = UserDefaults.standard
    let storedJson = userDefaults.value(forKeyPath: kbinPathKey) as? String
    if storedJson != nil {
        let storedData = GlobalFN().jsonToDict(jsonString: storedJson!) as!  Dictionary<String, AnyObject?>
        return storedData as! Dictionary<String, String>;
  
    }
    return [:]
}


func delAllBinFilePath(){
    let userDefaults: UserDefaults = UserDefaults.standard
    userDefaults.removeObject(forKey: kbinPathKey)
}

    
    func delBinFileEntry(filepath:String){
        
         let userDefaults: UserDefaults = UserDefaults.standard
          let storedJson = userDefaults.value(forKeyPath: kbinPathKey) as? String
        
          if storedJson != nil {
              let storedData = GlobalFN().jsonToDict(jsonString: storedJson!) as!  Dictionary<String, AnyObject?>
            var dic =  storedData as! Dictionary<String, String>;
               dic.removeValue(forKey: filepath)
        let jsonString = GlobalFN().dictToJSON(dict: dic as NSDictionary)
        userDefaults.set(jsonString, forKey: kbinPathKey)
        userDefaults.synchronize()
          }

    }

}
