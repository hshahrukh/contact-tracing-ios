//
//  traceData.swift
//  Contact Tracing
//
//  Created by ShahRukh on 03/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import Foundation


class Traces {
    var uuid:String = ""
    var timestamp:String = ""
    var other:String = ""

    init(uuid:String , timestamp:String , other:String){
        self.uuid = uuid
        self.timestamp = timestamp
        self.other = other

    }

}
