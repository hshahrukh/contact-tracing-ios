#!/usr/bin/python3
import re
from datetime import datetime


class ToBinary:
    def __init__(self, contact_trace,filename):
        self.contact_trace = contact_trace
        self.filename = filename

    def convert(self):
        traceFile = open(self.filename+".bin", "wb")
        epoch = datetime.utcfromtimestamp(0)

        for C in self.contact_trace:
            [uid160bit_str, ts_str] = C
            # Get rid of the dashes in uid160bit_str
            uid160bit_str = re.sub('-', '', uid160bit_str)
            # Write the UID bytes
            traceFile.write(bytes.fromhex(uid160bit_str))
            # Get the timestamp as a datetime_object
            datetime_object = datetime.strptime(ts_str, '%Y %b %d %H:%M:%S')
            # Convert timestamp to seconds since epoch
            ts_int = int((datetime_object - epoch).total_seconds())
            # Get bytes corresponding to int and write to file
            traceFile.write(ts_int.to_bytes(4, byteorder="big"))
            print(uid160bit_str)
            print(ts_str)
            print(ts_int)

            self.tricks.append(trick)
            return self
