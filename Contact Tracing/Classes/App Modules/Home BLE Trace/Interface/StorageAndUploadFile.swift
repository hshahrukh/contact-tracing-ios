//
//  StorageAndUploadFile.swift
//  Contact Tracing
//
//  Created by ShahRukh on 05/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//


import UIKit
//import CoreLocation
//import CoreBluetooth
import Toaster



//MARK: - Sending Data
extension TraceViewController
{
    
    
    
    
    @objc func syncDataPressed(){
        
        let alertController = UIAlertController (title: "", message:"Are you sure you want to upload data?", preferredStyle: .alert)
        
        let logoutAction = UIAlertAction(title: "Upload", style: .default, handler: {_ in
            self.uploadBinFiles()
        })
        alertController.addAction(logoutAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        // All done!
        
    }
    func deleteFile(url:URL){
        do {
            let fileManager = FileManager.default
            try fileManager.removeItem(atPath: url.path)
        }
        catch {
            
        }
    }
    
    func uploadBinFiles(){
        
        var bin_data =  TracePersistent().getBinFilePath()
        
        
        if bin_data.count == 0 {
            let alert: UIAlertController = UIAlertController(title: "", message: kNoBinFile, preferredStyle: .alert)
            let OKAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(OKAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
        for (bin_file_name, path) in bin_data{
            
            
            let fileManager = FileManager.default
            
            do {
                
                let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
                let url  = documentDirectory.appendingPathComponent(path)
                
                
                SwiftSpinner.show("Uploading Data.Please wait ...")
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                let ts_date = dateFormatter.string(from: Date())
                
                let filePath = url.path
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: filePath) {
                    
                    print("FILE AVAILABLE")
                    //                    continue;
                    
                } else {
                    print("FILE NOT AVAILABLE")
                    Toast(text:  "FILE NOT AVAILABLE"  , duration: Delay.short).show()
                    if let url = URL(string: path) {
                        deleteFile(url: url )
                    }
                    TracePersistent().delBinFileEntry(filepath:bin_file_name)
                    //                    bin_data.removeValue(forKey: bin_file_name)
                    
                    SwiftSpinner.hide()
                    continue;
                    
                }
                
                
                ApricotAPIManager.uploadBinaryFiles(data: [bin_file_name:url.absoluteString],param:["clock_time":ts_date as! AnyObject] ) { (netObject) in
                    
                    if netObject.isSuccess! {
                        
                        if netObject.statusCode! == NetworkResponseConstants.kSuccess {
                            
                            if let JSON = netObject.responseObject as? NSDictionary  {
                                SwiftSpinner.hide()
                                
                                if JSON["success"] as? Int == 1 {
                                    print("success");
                                    if let success_message = JSON["Msg"] as? String {
                                        
                                        
                                        if let url = URL(string: path){
                                            self.deleteFile(url: url )
                                        }
                                        TracePersistent().delBinFileEntry(filepath:bin_file_name)
                                        
                                        
                                        var bin_data_updated =  TracePersistent().getBinFilePath()
                                        
                                        if bin_data_updated.count == 0 {
                                            let alert: UIAlertController = UIAlertController(title: "Data has been successfully uploaded", message: success_message, preferredStyle: .alert)
                                            let OKAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                            alert.addAction(OKAction)
                                            self.present(alert, animated: true, completion: nil)
                                        }else {
                                            Toast(text:  success_message as! String , duration: Delay.short).show()
                                        }
                                        
                                    }
                                }
                                else {
                                    SwiftSpinner.hide()
                                    
                                    if let err = JSON["errors"] as? NSArray  {
                                        if  let errArr = err as? Array<String> {
                                            var alertErrMsg = ""
                                            for txt in 0..<errArr.count {
                                                alertErrMsg =  alertErrMsg + errArr[txt] + ", "
                                            }
                                            
                                            Toast(text:  alertErrMsg as! String , duration: Delay.long).show()
                                            
                                        }
                                            
                                        else if  let errArr = err[0] as? Array<String> {
                                            var alertErrMsg = ""
                                            for txt in 0..<errArr.count {
                                                alertErrMsg =  alertErrMsg + errArr[txt] + ", "
                                            }
                                            
                                            Toast(text:  alertErrMsg as! String , duration: Delay.long).show()
                                        }
                                    }
                                    print("error in param of some fields")
                                }
                                
                            }
                            
                            SwiftSpinner.hide()
                        }
                        else {   //API success not 200
                            SwiftSpinner.hide()
                            
                            if let status = netObject.statusCode {
                                let message = "Status code: \(status)." + GlobalFN().statusCodeNot200
                                Toast(text:  message  , duration: Delay.long).show()
                            }
                            return
                        }
                    }
                    else { //API failed
                        SwiftSpinner.hide()
                        
                        if let status = netObject.statusCode {
                                                       let message = "Status code: \(status)." + GlobalFN().statusCodeNot200
                                                       Toast(text:  message  , duration: Delay.long).show()
                                                   }
                        else {
                        Toast(text:   GlobalFN().unreachable  , duration: Delay.long).show()
                        
                        };
                        return
                    }
                }
                //            break;
            }catch{}
        }
    }
    
    
    
    func createBinFileEveryDay(){
        if GlobalFN().lastTimestampOfCreatingBinFile < 0 {
            GlobalFN().lastTimestampOfCreatingBinFile = Int64(Date().timeIntervalSince1970)
        }else {
            let currentTimeStamp = Int64(Date().timeIntervalSince1970)
            let lastTimeStamp = GlobalFN().lastTimestampOfCreatingBinFile
            
            if currentTimeStamp - lastTimeStamp > kTotalNumSecInDay { //if  is more than 1 day
                GlobalFN().lastTimestampOfCreatingBinFile = currentTimeStamp
                SwiftSpinner.show("Please Wait...")
                saveTraceDataToBinFile()
                SwiftSpinner.hide()
                
            }
        }
    }
    
    //
    //    func uploadTraceData(){
    //
    //    }
    
    func convertTraceDataAndSaveInBinaryFile(_ contact_trace: [[String]]) {
        fileURL = URL(fileURLWithPath: "");
        
        let fileManager = FileManager.default
        let fileNamed = "tdata_" + String(Int(Date().timeIntervalSince1970)) + ".bin"
        
        do {
            
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            fileURL = documentDirectory.appendingPathComponent(fileNamed)
            
            
            var globalData = Data()
            for elem in contact_trace {
                var uid160bit_str = elem[0]
                let ts_str = elem[1]
                uid160bit_str = uid160bit_str.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                
                let data = Data(fromHexEncodedString: uid160bit_str)!
                globalData.append(data)
                
                
//                let dateFormatter = DateFormatter()
//                            dateFormatter.dateFormat = "yyyy MMM dd HH:mm:ss"
//                            let ts_date = dateFormatter.date(from: ts_str)!
//                //
//                            let ts_int = Int32(TimeZone.current.secondsFromGMT(for: Date())) + Int32(ts_date.timeIntervalSince1970)
//                
                ////
                
                if  let ts_int = Int32(ts_str) {

                let array = withUnsafeBytes(of: ts_int.bigEndian, Array.init)
                let pointer1 = UnsafeBufferPointer(start:array, count:array.count )
                let data_time = Data(buffer:pointer1)
                
                globalData.append(data_time)
                }
                
            }
            do {
                try globalData.write(to: fileURL)
            }
            catch {/* error handling here */}
            
            
        } catch {
            print(error)
        }
        
        if self.fileURL.absoluteString.count > 0  {
            TracePersistent().saveBinFilePath(dataDict: [fileNamed:fileNamed])
            
        }
        
        var fileSize : UInt64
        do {
            let attr:NSDictionary? = try FileManager.default.attributesOfItem(atPath: self.fileURL.path) as NSDictionary
        if let _attr = attr {
            fileSize = _attr.fileSize();
            print("")
        }
        }
        catch {}
  
    }
    
    func deleteDataFilesOlderThan30Days(){
        let bin_data =  TracePersistent().getBinFilePath()
        
        for (bin_file_name, path) in bin_data {
            let fileNameArr = bin_file_name.components(separatedBy: "_")
            if fileNameArr.count > 1 {
                if let fileCreatedTimestamp = Int64(fileNameArr[0]) as? Int64 {
                    if  Int64(Date().timeIntervalSince1970) - fileCreatedTimestamp > kTotalNumSecInMonth {
                        if let url = URL(string: path){
                            self.deleteFile(url: url )
                        }
                        TracePersistent().delBinFileEntry(filepath:bin_file_name)
                    }
                }
            }
        }
    }
    
    
    func saveTraceDataToBinFile(){
        
        let alreadyBrosdcasting = self.broadcasting
        if alreadyBrosdcasting {
            self.broadcastAndReceiveBeacon(sender: self.traceButton)
        }
        
        self.locationManager.stopMonitoring(for: self.beaconRegion)
        self.locationManager.stopRangingBeacons(in: self.beaconRegion)
        let traces = db.read()
        
        var tracesProcessed = [[String]]()
        for i in 0..<traces.count{
            let trace = traces[i]
            let traceprocessith = [trace.uuid,trace.timestamp]
            tracesProcessed.append(traceprocessith)
        }
        deleteDataFilesOlderThan30Days()
        convertTraceDataAndSaveInBinaryFile(tracesProcessed)
        
        if self.fileURL.absoluteString.count > 0  {
            //AFTER API CALL ON SUCCESS
            db.deleteAllRows()
            var fileURL = URL(fileURLWithPath: "");
            if alreadyBrosdcasting {
                self.broadcastAndReceiveBeacon(sender: self.traceButton)
            }
        }
    }
    
    func apiToLogout() {
        SwiftSpinner.show("Logging out..")
        
        ApricotAPIManager.logout() { (netObject) in
            SwiftSpinner.hide()
            if netObject.isSuccess! {
                if netObject.statusCode! == NetworkResponseConstants.kSuccess {
                    if (netObject.responseObject as? NSDictionary) != nil  {
                        GlobalFN().token = ""
                        GlobalFN().user_id = ""
                        GlobalFN().pseudo_id_list = []
                        //change screen
                        let storyboard = UIStoryboard(name: "Login", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "loginVC") as! LoginViewController
                        self.pushViewControllerWithTransition(vc: vc)
                    }
                    
                    
                }
                else
                {
                    if let JSON = netObject.responseObject as? NSDictionary  {
                        
                        if JSON["error"] != nil {
                            let uialert = UIAlertController(title: "Could not Logout", message: "Problem with Logout!", preferredStyle: UIAlertController.Style.alert)
                            uialert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: nil))
                            self.present(uialert, animated: true, completion: nil)
                        }
                        else {
                            Toast(text: String(describing:
                                GlobalFN().statusCodeNot200), duration: Delay.short).show()
                        }
                    }
                }
            }
            else { //API failed
                
                
                if let status = netObject.statusCode {
                    let message = "Status code: \(status)." + GlobalFN().statusCodeNot200
                    Toast(text:message , duration: Delay.long).show()
                }
                else {
                    Toast(text:GlobalFN().unreachable , duration: Delay.long).show()
                }
                return
            }
            SwiftSpinner.hide()
            
        }
        
    }
    
    func pushViewControllerWithTransition(vc:UIViewController) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType(rawValue: "flip")
        transition.subtype = CATransitionSubtype.fromLeft
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(vc, animated: false)
        
    }
    
}
