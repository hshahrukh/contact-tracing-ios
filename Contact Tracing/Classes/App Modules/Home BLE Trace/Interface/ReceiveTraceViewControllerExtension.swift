//
//  ReceiveTraceViewControllerExtension.swift
//  Contact Tracing
//
//  Created by ShahRukh on 03/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import UIKit
import CoreLocation
import CoreBluetooth


// MARK: - Receiving

extension TraceViewController: CLLocationManagerDelegate
{
    
    
    func toggleScanning(sender:UIButton,start:Bool){
        
        // For Receiving
        beaconRegion = CLBeaconRegion(proximityUUID: pUUID, identifier: "apricot_beacons")
        beaconRegion.notifyEntryStateOnDisplay = true
        beaconRegion.notifyOnExit = true
        beaconRegion.notifyOnEntry = true
        
        if start {
            self.locationManager.startMonitoring(for: beaconRegion)
        }else {
            self.locationManager.stopMonitoring(for: beaconRegion)
            self.locationManager.stopRangingBeacons(in: beaconRegion)
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        guard status == .authorizedAlways else {
            print("******** User not authorized !!!!")
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion)
    {
        manager.requestState(for: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion)
    {
        if state == .inside {
            manager.startRangingBeacons(in: region as! CLBeaconRegion)
            return
        }
        
        //           manager.stopRangingBeacons(in: region as! CLBeaconRegion)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion){
        print("did enter")
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion){
        UserDefaults.standard.set("didEXIT", forKey: "Key") //setObject
        print("did exit")
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion)
    {
        
        if  Int64(Date().timeIntervalSince1970) - lastScanConsiderTime > 1 {
            lastScanConsiderTime = Int64(Date().timeIntervalSince1970)
            lastScanConsiderTime = Int64(Date().timeIntervalSince1970)
            
            self.beacons = beacons
            if self.beacons.count > 0 {
                for b in beacons {
                    
                    let major_str =   convertMajorMinorToHex(decimal: b.major.intValue)
                    let minor_str =   convertMajorMinorToHex(decimal: b.minor.intValue)
                    
                    let finaluuid = b.proximityUUID.uuidString + major_str + minor_str
//                    let ts_str = String(Int32(TimeZone.current.secondsFromGMT(for: Date())) + Int32(Date().timeIntervalSince1970))
                    let ts_str = String(Int32(Date().timeIntervalSince1970))

                    let traces = db.readdByID(uuid:finaluuid)
                    
                    if traces.count > 0 {
                       let trace_last = traces.last
                        if  (Int64(ts_str) ?? 0) - (Int64(trace_last?.timestamp ?? "0") ?? 0) < 60 { //log entry after 1 minute
                            db.insert(uuid: finaluuid, timestamp: ts_str, other: "")
                        }
                        
                    }else {
                        db.insert(uuid: finaluuid, timestamp: ts_str, other: "")
                    }
                }
            }
        }
    }
    
    func convertMajorMinorToHex(decimal: Int)-> String{
        var hexUnpadded = String(decimal, radix: 16)
        
        if kMajor_minor_hexLen - hexUnpadded.count > 0 {
            let fillingsize =  kMajor_minor_hexLen - hexUnpadded.count
            for _ in 0..<fillingsize {
                hexUnpadded = "0" + hexUnpadded
            }
        }
        return hexUnpadded
    }
    
}

