//
//  TraceViewController.swift
//  Contact Tracing
//
//  Created by ShahRukh on 02/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import UIKit
import CoreLocation
import CoreBluetooth
import Toaster

class TraceViewController: UIViewController,CBCentralManagerDelegate  {
    
    
    var db:DBHelper = DBHelper()
    var beaconRegion = CLBeaconRegion(proximityUUID:  iBeaconConfig.uuid, identifier: "apricot_beacons")
    var centralManager : CBCentralManager!
    var beacons: [CLBeacon] = []
    var currentAssignedIds:[String] = []
    var lastScanConsiderTime = Int64(Date().timeIntervalSince1970)
    var broadcasting: Bool = false
    var timerChangeUUID = Timer()
    var fileURL = URL(fileURLWithPath: "");
    
    //    var timerRecordBeacons = Timer()
    
    var beacon: CLBeaconRegion?
    var peripheralManager: CBPeripheralManager?
    let locationManager: CLLocationManager = CLLocationManager()
    
    @IBOutlet weak var traceButton: UIButton!
    var pUUID: UUID = iBeaconConfig.uuid
    
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func hexToStr(text: String) -> String {
        
        let regex = try! NSRegularExpression(pattern: "(0x)?([0-9A-Fa-f]{2})", options: .caseInsensitive)
        let textNS = text as NSString
        let matchesArray = regex.matches(in: textNS as String, options: [], range: NSMakeRange(0, textNS.length))
        let characters = matchesArray.map {
            Character(UnicodeScalar(UInt32(textNS.substring(with: $0.range(at: 2)), radix: 16)!)!)
        }
        return String(characters)
    }
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarChanges()
        let contact_trace = [ ["8888-8888-8888-8888-8888-8888-8888-8888-0000-0001", "2020 Apr 2 21:46:31" ],
        ["8888-8888-8888-8888-8888-8888-8888-8888-0002-0003", "2020 Apr 2 21:46:34" ],
        ["8888-8888-8888-8888-8888-8888-8888-8888-a001-b002", "2020 Apr 2 21:46:37" ],
        ["8888-8888-8888-8888-8888-8888-8888-8888-e003-f004", "2020 Apr 2 21:46:40" ],
        ["8888-8888-8888-8888-8888-8888-8888-8888-01ab-cd20", "2020 Apr 2 21:46:43" ] ]
//        convertTraceDataAndSaveInBinaryFile(contact_trace)
        
        //        currentAssignedIds.append("888888888888888888888888888888880001000f")
        //        currentAssignedIds.append("8888888888888888888888888888888800020002")
        //        currentAssignedIds.append("88888888888888888888888888888888000f0001")
        //
        currentAssignedIds = GlobalFN().pseudo_id_list as! [String]
        createBinFileEveryDay()
        
        
        self.locationManager.delegate = self
        centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
        
        
        self.peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        
        
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates=true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        
        if #available(iOS 11.0, *) {
            self.locationManager.showsBackgroundLocationIndicator=true
        } else {
            // Fallback on earlier versions
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    func navigationBarChanges(){
        self.navigationController?.setNavigationBarHidden(false, animated: true) //Hide
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "logout"), for: .normal)
        button.addTarget(self, action:#selector(logoutPressed), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
        
        
        let buttonsync = UIButton(type: UIButton.ButtonType.custom)
        buttonsync.setImage(UIImage(named: "sync"), for: .normal)
        buttonsync.addTarget(self, action:#selector(syncDataPressed), for: .touchUpInside)
        buttonsync.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButtonsync = UIBarButtonItem(customView: buttonsync)
        self.navigationItem.rightBarButtonItem = barButtonsync
        
        
        
    }
    

    
    
    deinit
    {
        self.beacon = nil
        self.peripheralManager = nil
    }
    
    @objc func logoutPressed(){
        print("logout pressed")
        
        
        let alertController = UIAlertController (title: "", message:"Are you sure you want to exit?", preferredStyle: .alert)
        
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler: {_ in
            self.apiToLogout()
        })
        alertController.addAction(logoutAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func broadcastAndReceiveBeacon(sender: UIButton){
        
        
        toggleBroadcasting(sender: sender);
        // For Broadcasting
        
        
        
        
        let animations: () -> Void = {
            if #available(iOS 13.0, *) {
                let backgroundColor: UIColor = (self.broadcasting) ? UIColor.systemBackground : UIColor.systemBlue
                self.view.backgroundColor = backgroundColor
                
            } else {
                let backgroundColor: UIColor = (self.broadcasting) ? UIColor.white : UIColor.blue
                // Fallback on earlier versions
                self.view.backgroundColor = backgroundColor
                
            }
            
            
            self.broadcasting = !self.broadcasting
            self.setNeedsStatusBarAppearanceUpdate()
        }
        
        let completion: (Bool) -> Void = {
            finish in
            self.advertising(start: self.broadcasting)
            self.toggleScanning(sender: sender,start: self.broadcasting);
            if !self.broadcasting{
                self.timerChangeUUID.invalidate()
                //                self.timerRecordBeacons.invalidate()
            }else {
                self.timerChangeUUID = Timer.scheduledTimer(timeInterval: TimeInterval(5*kTotalNumSecInMin), target: self, selector: #selector(self.advertisingAfterXIntervalWithChangeUUID), userInfo: nil, repeats: true)
                
            }
        }
        
        UIView.animate(withDuration: 0.25, animations: animations, completion: completion)
        
        
        
        
    }
    
    
    
}


extension TraceViewController
{
    
    @IBAction func powerSaveModeStart(sender:UIButton){
        
        if sender.isSelected == false {
            sender.isSelected = true
            sender.setTitle("Turn Power Mode Off", for: .normal)
            if ProcessInfo.processInfo.isLowPowerModeEnabled {
                // Low Power Mode is enabled. Start reducing activity to conserve energy.
                
                
            } else {
                
                let alertController = UIAlertController (title: "Low Power Mode", message: "As Apricot works best when in foreground, so to save battery please switch to low power mode. To turn Low Power Mode on, go to Settings > Battery ", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(cancelAction)
                
                present(alertController, animated: true, completion: nil)
                
                // Low Power Mode is not enabled.
            }
            
            if #available(iOS 13.0, *) {
                _ = UIApplication.shared.delegate as! AppDelegate
                // appDelegate.window?.overrideUserInterfaceStyle = .dark
                self.overrideUserInterfaceStyle = .dark
            }
            UIScreen.main.brightness = CGFloat(0.01)
            UIApplication.shared.isIdleTimerDisabled = true
            
            
            
        }else {
            sender.setTitle("Turn Power Mode ON", for: .normal)
            
            sender.isSelected = false
            UIScreen.main.brightness = CGFloat(0.5)
            UIApplication.shared.isIdleTimerDisabled = false
            if #available(iOS 13.0, *) {
                _ = UIApplication.shared.delegate as! AppDelegate
                self.overrideUserInterfaceStyle = .light
                //appDelegate.window?.overrideUserInterfaceStyle = .light
                
            } else {
                // Fallback on earlier versions
            }
        }
    }
}





