//
//  BroadTraceViewControllerExtension.swift
//  Contact Tracing
//
//  Created by ShahRukh on 03/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import UIKit
import CoreLocation
import CoreBluetooth


// MARK: - Broadcasting

extension TraceViewController: CBPeripheralManagerDelegate
{
    
    
    
    func toggleBroadcasting(sender: UIButton){
        
        
        let state: CBManagerState = self.peripheralManager!.state
        
        if (state == .poweredOff && !self.broadcasting) {
            let OKAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            let alert: UIAlertController = UIAlertController(title: "Bluetooth OFF", message: "Please power on your Bluetooth!", preferredStyle: .alert)
            alert.addAction(OKAction)
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        let titleFromStatus: () -> String = {
            let title: String = (self.broadcasting) ? "Start" : "Stop"
            
            return title + " Tracing"
        }
        
        if #available(iOS 13.0, *) {
            let buttonTitleColor: UIColor = (self.broadcasting) ? UIColor.systemBackground : UIColor.systemBlue
            sender.setTitleColor(buttonTitleColor, for: .normal)
            
        } else {
            let buttonTitleColor: UIColor = (self.broadcasting) ? UIColor.white : UIColor.blue
            // Fallback on earlier versions
            sender.setTitleColor(buttonTitleColor, for: .normal)
            
        }
        
        sender.setTitle(titleFromStatus(), for: .normal)
        
        let _: () -> String = {
            let text: String = (self.broadcasting) ? "Not Broadcast" : "Broadcasting..."
            
            return text
        }
        
        //               self.statusLabel.text = labelTextFromStatus()
        
        
        
    }
    
    
    
    
    func pickRandUUID() -> String{
        let currentAssignedId = currentAssignedIds.randomElement() ?? ""
        if currentAssignedId.count >= 32 {
            
        let uuid_invalid = String(currentAssignedId.prefix(32))

//        let valid_uuid_2 = uuid_invalid.substring(to: 8) + "-"
        let valid_uuid = uuid_invalid.substring(to: 8) + "-" + uuid_invalid.substring(from: 8).substring(to: 4) + "-" + uuid_invalid.substring(from: 12).substring(to: 4) + "-" + uuid_invalid.substring(from: 16).substring(to: 4) + "-" + uuid_invalid.substring(from: 20)

        
        if let uuid_updated =  UUID(uuidString: valid_uuid) {
            pUUID = uuid_updated
        }
        }
        return currentAssignedId
    }
    
    // MARK: - Broadcast Beacon
    
    
    
    
    
    @objc  func advertisingAfterXIntervalWithChangeUUID(){
        createBinFileEveryDay()
        advertising(start: false)
        advertising(start: true)
    }
    
    func advertising(start: Bool) -> Void
    {
        if self.peripheralManager == nil {
            return
        }
        
        if (!start) {
            self.peripheralManager!.stopAdvertising()
            return
        }
        
        let state: CBManagerState = self.peripheralManager!.state
        
        let currentAssignedId =   self.pickRandUUID()

        
        
        if (state == .poweredOn) {
            
        
            if currentAssignedId.count > 30 {
                let endIndex32 = currentAssignedId.index(currentAssignedId.endIndex, offsetBy: -8)
                let last32bit  = currentAssignedId.substring(from: endIndex32)
                
//                let startIndex128 = currentAssignedId.index(currentAssignedId.startIndex, offsetBy: 32)
//                let first128bit = currentAssignedId.substring(from: startIndex128)
//
                
                let endIndex16 = last32bit.index(last32bit.endIndex, offsetBy: -4)
                let minorStr  = last32bit.substring(from: endIndex16)
                
                
                let startIndex16 = last32bit.index(last32bit.startIndex, offsetBy: 4)
                let  majorStr  = last32bit.substring(to: startIndex16)
                if let majorUint = UInt(String(majorStr), radix: 16), let minorUint = UInt(String(minorStr), radix: 16)  {
                    
                    
                    
                    let major: CLBeaconMajorValue = CLBeaconMajorValue(majorUint)
                    let minor : CLBeaconMinorValue = CLBeaconMinorValue(minorUint)
                    
                    
                    print("MAJOR:"+String(majorStr)+" Minor:"+String(minorStr));
                    
                    
//                    let data = Data(String(first128bit).utf8)
//                    let hexString = data.map{ String(format:"%02x", $0) }.joined()
//                    
                   
                    
                    self.beacon = CLBeaconRegion(proximityUUID: pUUID, major: major, minor: minor, identifier: "beacon_123")
                    
                    
                    let UUID:UUID = (self.beacon?.proximityUUID)!
                    let serviceUUIDs: Array<CBUUID> = [CBUUID(nsuuid: UUID)]
                    
                    // Why NSMutableDictionary can not convert to Dictionary<String, Any> 😂
                    var peripheralData: Dictionary<String, Any> = self.beacon!.peripheralData(withMeasuredPower: 1)  as NSDictionary as! Dictionary<String, Any>
                    peripheralData[CBAdvertisementDataLocalNameKey] = "Apricot_123"
                    peripheralData[CBAdvertisementDataServiceUUIDsKey] = serviceUUIDs
                    self.peripheralManager!.startAdvertising(peripheralData)
                    
                }
            }
        }
        
    }
    
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("");
        if central.state == .poweredOn {
            print("Bluetooth is On")
            centralManager.scanForPeripherals(withServices: nil, options: nil)
        } else {
            print("Bluetooth is not active")
        }
    }
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager)
    {
        let state: CBManagerState = peripheralManager!.state
        
        if state == .poweredOff {
            if self.broadcasting {
                self.broadcastAndReceiveBeacon(sender: self.traceButton)
            }
        }
        
        if state == .unsupported {
            //                self.statusLabel.text = "Unsupported Beacon"
        }
        
        if state == .poweredOn {
            //                self.statusLabel.text = "Not Broadcast"
        }
    }
    
    
}





