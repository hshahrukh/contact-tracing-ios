//
//  ViewController.swift
//  Contact Tracing
//
//  Created by ShahRukh on 31/03/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import UIKit
import Toaster

class LoginViewController: UIViewController {
    @IBOutlet weak var editPhoneNumButton: UIButton!
    
    @IBOutlet weak var phoneNumberInput: UITextField!
    @IBOutlet weak var getOTPButton: UIButton!
    
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var otpSentText: UILabel!
    @IBOutlet weak var buildDate: UILabel!
    @IBOutlet weak var appVersion: UILabel!
    var otpTimer = Timer()
    override func viewWillAppear(_ animated: Bool) {
        if(GlobalFN().token != "") {
            let storyboard = UIStoryboard(name: "Trace", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TraceVC") as! TraceViewController
            self.pushViewControllerWithTransition(vc: vc)
        }
        super.viewWillAppear(true)
        appVersion.text = GlobalFN().appVersion
        buildDate.text = GlobalFN.buildDate
        getOTPButton.addTarget(self, action: #selector(getOTP), for: .touchUpInside)
        phoneNumberInput.addTarget(self, action: #selector(phoneNumFieldDidChange), for: UIControl.Event.editingChanged)
        
        resendButton.addTarget(self, action: #selector(resendOTP), for: .touchUpInside)
        resendButton.isHidden = true
        otpSentText.isHidden = true
        editPhoneNumButton.addTarget(self, action: #selector(loadPhoneNumberScreen), for: .touchUpInside)
        editPhoneNumButton.isHidden = true
        
        getOTPButton.isEnabled = false
        getOTPButton.alpha = 0.5
        phoneNumberInput.becomeFirstResponder()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true) //Hide


        // Do any additional setup after loading the view.
    }
    
    @objc func getOTP(){
    let phoneNumber = phoneNumberInput.text!
        GlobalFN().phoneNum =  phoneNumber
        apiToGenerateOTP()
    }
    
    @objc func phoneNumFieldDidChange(){
        var phoneNumberCount = 0
        if let phoneNumber = phoneNumberInput.text {
            phoneNumberCount = phoneNumber.count
        }
        if(phoneNumberCount < 10) {
            getOTPButton.isEnabled = false
            getOTPButton.alpha = 0.5
        }else {
            if(phoneNumberCount > 10) {
                phoneNumberInput.deleteBackward()
            }
            getOTPButton.isEnabled = true
            getOTPButton.alpha = 1
        }
    }
    func waitForOTP() {
        otpSentText.text = "OTP SENT TO " + GlobalFN().phoneNum
        otpSentText.isHidden = false;
        
        editPhoneNumButton.isHidden = false

        resendButton.isHidden = false
        resendButton.isEnabled = false
        
        getOTPButton.isEnabled = false
        getOTPButton.alpha = 0.5
        getOTPButton.removeTarget(self, action: #selector(getOTP), for: .touchUpInside)
        getOTPButton.addTarget(self, action: #selector(checkOTPAndLogin), for: .touchUpInside)
        getOTPButton.setTitle("LOGIN", for: .normal)
        
        phoneNumberInput.removeTarget(self, action: #selector(phoneNumFieldDidChange), for: UIControl.Event.editingChanged)
        phoneNumberInput.addTarget(self, action: #selector(OTPInputDidChange), for: UIControl.Event.editingChanged)
        phoneNumberInput.placeholder = "Enter OTP"
        phoneNumberInput.text = ""
        
        otpTimer = Timer.scheduledTimer(timeInterval: TimeInterval(GlobalFN().OTP_MIN_INTERVAL), target: self, selector: #selector(enableResendOTP), userInfo: nil, repeats: false)
        SwiftSpinner.hide()
    }
    
    @objc func OTPInputDidChange() {
        var OTPCount = 0
        if let OTP = phoneNumberInput.text {
            OTPCount = OTP.count
        }
        if(OTPCount < GlobalFN().OTP_LENGTH) {
            getOTPButton.isEnabled = false
            getOTPButton.alpha = 0.5
        }else {
            if(OTPCount > GlobalFN().OTP_LENGTH) {
                phoneNumberInput.deleteBackward()
            }
            getOTPButton.isEnabled = true
            getOTPButton.alpha = 1
        }
    }
    
    @objc func checkOTPAndLogin() {
        let otp = phoneNumberInput.text! //Since same input is used for phone number and otp
        apiToVerifyOTP(otp: otp)
    }
    
    @objc func enableResendOTP() {
        resendButton.isEnabled = true
    }
    
    
    @objc func loadPhoneNumberScreen() {
        GlobalFN().OTP = ""
        otpSentText.isHidden = true
        
        editPhoneNumButton.isHidden = true
        
        resendButton.isHidden = true
        otpTimer.invalidate()
        
        getOTPButton.removeTarget(self, action: #selector(checkOTPAndLogin), for: .touchUpInside)
        getOTPButton.addTarget(self, action: #selector(getOTP), for: .touchUpInside)
        getOTPButton.setTitle("GET OTP", for: .normal)
        getOTPButton.isEnabled = true
        getOTPButton.alpha = 1
        phoneNumberInput.removeTarget(self, action: #selector(OTPInputDidChange), for: UIControl.Event.editingChanged)
        phoneNumberInput.addTarget(self, action: #selector(phoneNumFieldDidChange), for: UIControl.Event.editingChanged)
        phoneNumberInput.placeholder = "Enter Phone Number.."
        phoneNumberInput.text = GlobalFN().phoneNum

    }
    
    @objc func resendOTP() {
        apiToGenerateOTP()
        resendButton.isEnabled = false
        otpTimer = Timer.scheduledTimer(timeInterval: TimeInterval(GlobalFN().OTP_MIN_INTERVAL), target: self, selector: #selector(enableResendOTP), userInfo: nil, repeats: false)
        
    }
    
   
    func apiToGenerateOTP() {
        SwiftSpinner.show("Sending OTP")
        
        let parameters : [String : String] = [
            "phone" : GlobalFN().phoneNum as String,
            "device_id" :    GlobalFN().keychainUniqueDeviceId,
            "app_id" : GlobalFN().app_id
        ]
        
        let jsonString = GlobalFN().dictToJSON(dict: parameters as NSDictionary)
        let jsonData = jsonString.data(using: .utf8)!
        let attListRequest = try! JSONDecoder().decode(otpRequest.self, from: jsonData)
        
        SwiftSpinner.show("Please Wait...")
        
        ApricotAPIManager.generateOTP(lastUpdateRQ:attListRequest) { (netObject) in
            SwiftSpinner.hide()
            
            if netObject.isSuccess! {
                if netObject.statusCode! == NetworkResponseConstants.kSuccess {
                    
                    if (netObject.responseObject as? NSDictionary) != nil  {
                        print("OTP-SENT-SUCCESSFULLY")
                        self.waitForOTP()
                    }
                    
                    
                }
                else
                {
                    if let JSON = netObject.responseObject as? NSDictionary  {
                        
                        if JSON["detail"] != nil {
                            Toast(text: String(describing: JSON["detail"]!), duration: Delay.short).show()
                        }
                        else {
                          Toast(text: String(describing:
                            GlobalFN().statusCodeNot200), duration: Delay.short).show()
                        }
                    }
                }
            }
            else { //API failed
                
                
                if let status = netObject.statusCode {
                    let message = "Status code: \(status)." + GlobalFN().statusCodeNot200
                    Toast(text:message , duration: Delay.long).show()
                }
                else {
                    Toast(text:GlobalFN().unreachable , duration: Delay.long).show()
                }
                return
            }
            SwiftSpinner.hide()
            
        }
    }
    
    func pushViewControllerWithTransition(vc:UIViewController) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType(rawValue: "flip")
        transition.subtype = CATransitionSubtype.fromLeft
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(vc, animated: false)

    }
    
    func apiToVerifyOTP(otp: String) {
        SwiftSpinner.show("Verifying OTP..")
        let parameters : [String : String] = [
            "phone" : GlobalFN().phoneNum,
            "device_id" : GlobalFN().keychainUniqueDeviceId,
            "app_id" : GlobalFN().app_id,
            "otp" : otp
        ]
        let jsonString = GlobalFN().dictToJSON(dict: parameters as NSDictionary)
        let jsonData = jsonString.data(using: .utf8)!
        let attListRequest = try! JSONDecoder().decode(otpVerifyRequest.self, from: jsonData)
        
        SwiftSpinner.show("Please Wait ...")
        
        ApricotAPIManager.verifyOTP(lastUpdateRQ:attListRequest) { (netObject) in
                   SwiftSpinner.hide()
                   
                   if netObject.isSuccess! {
                       if netObject.statusCode! == NetworkResponseConstants.kSuccess {
                           if let JSON = netObject.responseObject as? NSDictionary  {
                            if let value = JSON["value"] {
                                let valueDict = value as! NSDictionary
                                GlobalFN().token = valueDict["token"] as! String
                                GlobalFN().user_id = valueDict["user_id"] as! String
                                GlobalFN().pseudo_id_list = valueDict["pseudo_id_list"] as! NSArray
                                //change screen
                               let storyboard = UIStoryboard(name: "Trace", bundle: nil)
                               let vc = storyboard.instantiateViewController(withIdentifier: "TraceVC") as! TraceViewController
                               self.pushViewControllerWithTransition(vc: vc)
                           }
                        }
                           
                           
                       }
                       else
                       {
                           if let JSON = netObject.responseObject as? NSDictionary  {
                               
                            if JSON["error"] != nil {
                                    let uialert = UIAlertController(title: "Incorrect OTP", message: "The OTP you entered is incorrect!", preferredStyle: UIAlertController.Style.alert)
                                       uialert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(uialert, animated: true, completion: nil)
                                }
                               else {
                                 Toast(text: String(describing:
                                   GlobalFN().statusCodeNot200), duration: Delay.short).show()
                               }
                           }
                       }
                   }
                   else { //API failed
                       
                       
                       if let status = netObject.statusCode {
                           let message = "Status code: \(status)." + GlobalFN().statusCodeNot200
                           Toast(text:message , duration: Delay.long).show()
                       }
                       else {
                           Toast(text:GlobalFN().unreachable , duration: Delay.long).show()
                       }
                       return
                   }
                   SwiftSpinner.hide()
                   
               }
    
        
    }
    

    
}

