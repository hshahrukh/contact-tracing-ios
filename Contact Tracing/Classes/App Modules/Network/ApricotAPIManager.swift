//
//  MyPolicyAPIManager.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//
import Foundation

class ApricotAPIManager: NSObject {
    

        static func getJSONString(inputParams : Any){
            let jsonData = try! JSONSerialization.data(withJSONObject: inputParams, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            print(jsonString)
        }
 

    static func generateOTP(lastUpdateRQ: otpRequest, callback: @escaping NetworkClosure) {
              
          let inputParams =  lastUpdateRQ.toDict()

          let jsonData = try! JSONSerialization.data(withJSONObject: inputParams, options: JSONSerialization.WritingOptions.prettyPrinted)
          let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
          
          
          
          print(jsonString)
          let requestModel = SafeRequestModal.init(requestIdentifier: NetworkOperations.kGenerateOTP, IParams: inputParams as Dictionary<String, AnyObject>, UParams: nil)
          let appNetwork = ApricotNetwork.sharedDashboardNetworkInstance
          appNetwork.getNetworkResponseFor(request: requestModel, callback: callback)
              
          }
    
    static func verifyOTP(lastUpdateRQ: otpVerifyRequest, callback: @escaping NetworkClosure) {
            
        let inputParams =  lastUpdateRQ.toDict()

        let jsonData = try! JSONSerialization.data(withJSONObject: inputParams, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        
        
        
        print(jsonString)
        let requestModel = SafeRequestModal.init(requestIdentifier: NetworkOperations.kVerifyOTP, IParams: inputParams as Dictionary<String, AnyObject>, UParams: nil)
    //    requestModel.requiresToken()
        let appNetwork = ApricotNetwork.sharedDashboardNetworkInstance
        appNetwork.getNetworkResponseFor(request: requestModel, callback: callback)
            
        }
    
    static func logout(callback: @escaping NetworkClosure) {
            
        let inputParams =  [String: String]()

           let jsonData = try! JSONSerialization.data(withJSONObject: inputParams, options: JSONSerialization.WritingOptions.prettyPrinted)
           let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
           
           
           
           print(jsonString)
           let requestModel = SafeRequestModal.init(requestIdentifier: NetworkOperations.kLogout, IParams: inputParams as Dictionary<String, AnyObject>, UParams: nil)
           requestModel.requiresToken()
           let appNetwork = ApricotNetwork.sharedDashboardNetworkInstance
           appNetwork.getNetworkResponseFor(request: requestModel, callback: callback)
               
           }
    
    
    static func uploadBinaryFiles(data: Dictionary<String,String>,param:Dictionary<String,AnyObject> , callback: @escaping NetworkClosure) {
        
        var inputParams = Dictionary<String, Any>()
        
        
        let jsonData = try! JSONSerialization.data(withJSONObject: inputParams, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        
        print(jsonString)
        let requestModel = SafeRequestModal.init(requestIdentifier: NetworkOperations.kUploadData, IParams: param as? Dictionary<String, AnyObject>, UParams: nil)
        
        requestModel.data_files = data
        requestModel.requiresToken()
        requestModel.isMultiPart = true
        let appNetwork = ApricotNetwork.sharedDashboardNetworkInstance
        appNetwork.getNetworkResponseFor(request: requestModel, callback: callback)
        
    }
        
}
