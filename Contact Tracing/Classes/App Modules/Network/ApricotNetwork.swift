//
//  MyPolicyNetwork.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation
import UIKit


class ApricotNetwork: AppNetwork {
    
    static let sharedDashboardNetworkInstance = ApricotNetwork()
    
    override init() {
        super.init()
        let networkFilePath = Bundle.main.path(forResource: "ApricotNetworkURL", ofType: "plist")
        print(networkFilePath ?? "network file path empty")
        self.requestConstructor = SafeRequestConstructor.init(filePath: networkFilePath!)
    }
}
