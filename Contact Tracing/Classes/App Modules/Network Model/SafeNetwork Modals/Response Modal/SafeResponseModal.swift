//
//  SafeResponseModal.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation

struct SafeNetObject {
    
    let responseObject:      AnyObject?
    let operationSuccessful: Bool?
    let serverMessage:       String?
    var responseString:      String?
    
    let statusCode: Int?
    let error:      NSError?
    var isSuccess:  Bool?
    
    init(response: AnyObject?, status: Int?, err:NSError?, successful: Bool?, opSuccessful: Bool?, message: String?) {
        
        self.responseObject = response
        self.statusCode = status
        self.error = err
        self.isSuccess = successful
        self.operationSuccessful = opSuccessful
        self.serverMessage = message
    }
    
}
