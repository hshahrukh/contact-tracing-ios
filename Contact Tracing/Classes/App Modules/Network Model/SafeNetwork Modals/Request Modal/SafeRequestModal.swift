//
//  SafeRequestModal.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation


class SafeRequestModal: NSObject {
    
    var inputParameters: Dictionary<String, AnyObject>?
    var urlParameters  : [String: String]?
    
    var requestType = RequestType.POST
    var baseUrlL: String?
    var operationUrl: String?
    var urlIdentifier: String?
    var spinnerText: String?
    var modalClassName: String?

    
    var requestHeader: [String: String]?
    var cachesData: Bool = false
    var isLoadStub: Bool = false
    
    var isMultiPart: Bool = false
    var data_files:Dictionary<String,String>?
    
    var images:Dictionary<String,Data>?
    
    
//    var fileTransaction : ASFileTransaction?
    var tokenRequired: Bool = false
    var consumeNetworkError = false
    
    override init() {
        
        self.inputParameters = nil
        self.urlParameters = nil
        self.baseUrlL = nil
        self.operationUrl = nil
        self.urlIdentifier = nil
        self.spinnerText = nil
        self.modalClassName = nil
        self.requestHeader = nil
    }
    
    init(requestIdentifier: String, IParams: Dictionary<String, AnyObject>?, UParams: Dictionary<String,String>?) {
    
        self.inputParameters = IParams
        self.urlParameters = UParams
        self.urlIdentifier = requestIdentifier
        self.baseUrlL = nil
        self.operationUrl = nil
        self.spinnerText = nil
        self.modalClassName = nil
        self.requestHeader = nil
    }
    
    
//    func requestFileOperation(isUpload: Bool = false , filePath:String? , uploadComps: Dictionary<String,AnyObject>?){
//        self.fileTransaction = ASFileTransaction(isUpload: isUpload, fPath: filePath, uploadComps: uploadComps)
//    }
    
    func requiresToken() {
        self.tokenRequired = true
    }
    
    func consumesNetworkError() {
        self.consumeNetworkError = true
    }
    
    
    func getAuthorizationHeader()-> String?{
        
        if let returnValue = UserDefaults.standard.object(forKey: "token") as? String {
            return returnValue
        }
        return nil
    }
    
    
//    func getAppVersion()-> String?{
//        
//        return GlobalFN().appVersion
//
//    }
// 
    
  
    
}


