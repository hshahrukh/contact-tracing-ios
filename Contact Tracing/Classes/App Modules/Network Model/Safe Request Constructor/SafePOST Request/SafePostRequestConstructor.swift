//
//  SafePOSTRequestConstructor.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation
import UIKit
import Alamofire



struct SafePOSTRequestConstructor {
    
    static func getResponseForRequest(request: SafeRequestModal, callback:@escaping NetworkClosure) {
        guard request.baseUrlL != nil && request.operationUrl != nil && request.baseUrlL?.count ?? 0 > 0  && request.operationUrl?.count ?? 0 > 0 else {
            return
        }
        
        
        let url = request.baseUrlL! + request.operationUrl!
//        if  request.urlIdentifier ==  NetworkOperations.kUploadData {
//          removeCookies(url: url)
//        }

        URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)

        Alamofire.request(url, method: .post, parameters: request.inputParameters, encoding: JSONEncoding.default , headers: request.requestHeader).responseJSON { (response) in
            
            #if DEBUG
                
                print("********r**************** Request Start**************************");
                print(response.request!)  // original URL request
                print("************************ Request End **************************");
                print("************************ Response Data **************************");
                let data = response.data
                var datastring = "";
                if(data != nil){
                    
                    if String(data: data!, encoding: String.Encoding.utf8) != nil {
                        datastring = String(data: data!, encoding: String.Encoding.utf8)!
                    } else {
                        
                    }
                    print(datastring)
                    
                }else{
                    print("\n\n valid response not found");
                }
                
                print("************************ Response Data END **************************");
                
                print("************************ Response Results **************************");
                print(response.result)   // result of response serialization
                print("************************ Response Results END **************************");
                
            #endif
            
           
            
            let operationSuccessful: Bool = true
            let message: String? = nil
            
            
           
            
            
            var responseObj = SafeNetObject.init(response: response.result.value as AnyObject, status: response.response?.statusCode, err: response.result.error as NSError?, successful: response.result.isSuccess, opSuccessful: operationSuccessful, message: message)
            if request.cachesData == true {
                let data = response.data
                var dataString = ""
                if data != nil {
                    dataString = String(data: data!, encoding: String.Encoding.utf8)!
                }
                responseObj.responseString = dataString
            }
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
       //     if GlobalFN().updateApp(responseObj.responseObject , (appDelegate.window!.rootViewController)!) {
         //       return
          //  }
            let urlt = request.baseUrlL! + request.operationUrl!
            
            if  request.urlIdentifier ==  NetworkOperations.kUserLogin {
                setcookies(url: urlt)
            }
            
            callback(responseObj)
            
        }
    }
    
    static func fetchFileNameFromHeader(header:Dictionary<AnyHashable, Any>?)->String?{
        
        let _ = header?["fileType"]
        if let fileName = header?["Content-Disposition"] as? String{
            let fileComp = fileName.components(separatedBy: ";")
            if(fileComp.count == 2){
                let fileNameComp = fileComp[1]
                let nameComps = fileNameComp.components(separatedBy: "=")
                
                var urlString = nameComps.last!
                
                if !nameComps.last!.contains("pdf") {
                    let name = nameComps.last! + ".pdf"
                    return name
                } else {
                    let urlArray = nameComps.last!.components(separatedBy: ".")
                    if urlArray.count >= 2 {
                        urlString = urlArray[0] + "." + urlArray[1]
                    }
                }
                
                print(urlString)
                let testurlString = urlString.replacingOccurrences(of: "\"", with: "", options: .literal, range: nil)
                return testurlString
            }
        }
        return nil
    }
    
    
   static func removeCookies( url: String) {
            let cstorage = HTTPCookieStorage.shared
            if let cookies = cstorage.cookies(for: URL.init(string: url)!) {
                for cookie in cookies {
                  let t =   cookie as HTTPCookie
//                    if t.name == "csrftoken" {
//                        GlobalFN().crtfToken = t.value
//                    }
                    cstorage.deleteCookie(cookie)
                }
            }
    }
    
    
 static  func  setcookies(url:String) {
    let cstorage = HTTPCookieStorage.shared
    if let cookies = cstorage.cookies(for: URL.init(string: url)!) {
        for cookie in cookies {
            let t =   cookie as HTTPCookie
                                if t.name == "csrftoken" {
                                   // GlobalFN().crtfToken = t.value
                                }
         //   cstorage.deleteCookie(cookie)
        }
    }
    }
    
    
    static func getResponseForMultipartRequest(_ request: SafeRequestModal, callback: @escaping NetworkClosure){
        
        let url =  request.baseUrlL! + request.operationUrl! ;
        
        if  request.urlIdentifier ==  NetworkOperations.kUploadData {
          removeCookies(url: url)
        }
        
        request.requestHeader?["Content-Type"] = "multipart/form-data"
        request.requestHeader?["Content-type"] = "multipart/form-data"


        Alamofire.upload(multipartFormData:{ multipartFormData in
            if(request.images != nil){
                for (key, value) in request.images!{
                    multipartFormData.append(value, withName: request.inputParameters?["key"] as! String, fileName: key, mimeType: "image/jpg")
                }
                request.inputParameters?.removeValue(forKey: "key")
            }
            
            if (request.data_files != nil){
                
                for (key, value) in request.data_files!{
                    let url = URL(string: value)!
                    
                    do {
                             let attr:NSDictionary? = try FileManager.default.attributesOfItem(atPath: url.path) as NSDictionary
                         if let _attr = attr {
                            let fileSize = _attr.fileSize();
                             print("")
                         }
                    }catch{}
                    
                    multipartFormData.append(url, withName: key)
         
                }
            }
            
            
            if(request.inputParameters != nil){
                let jsonData = try! JSONSerialization.data(withJSONObject: request.inputParameters!, options: JSONSerialization.WritingOptions.prettyPrinted)
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                for (key,value) in request.inputParameters! {
                    let value_str = value as! String
                multipartFormData.append(value_str.data(using: .utf8)!, withName:key)
                }
            }
        },
                         usingThreshold:UInt64.init(),
                         to:url,
                         method:.post,
                         headers:request.requestHeader,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    // debugPrint(response)
                                    
                                    let data = response.data
                                    var datastring = "";
                                    if(data != nil){
                                        
                                        if String(data: data!, encoding: String.Encoding.utf8) != nil {
                                            datastring = String(data: data!, encoding: String.Encoding.utf8)!
                                        } else {
                                            
                                        }
                                        print(datastring)
                                        
                                    }else{
                                        print("\n\n valid response not found");
                                    }
                                    
                                    print("************************ Response Data END **************************");
                                    
                                    print("************************ Response Results **************************");
                                    print(response.result)   // result of response serialization
                                    print("************************ Response Results END **************************");
                                    print("test");
                                    
                                    
                                    let responseObj = SafeNetObject.init(response: response.result.value as AnyObject?, status: response.response?.statusCode, err: response.result.error as NSError?, successful:response.result.isSuccess, opSuccessful: true, message: "")
                                    callback(responseObj);
                                    
                                }
                            case .failure(let encodingError):
                                let responseObj = SafeNetObject.init(response: nil, status: 400, err: encodingError as NSError?, successful:false, opSuccessful: false, message: "")
                                callback(responseObj);
                                
                            }
                            
        })
        
    }

    
}

//"BRT101.1781NU1.001575988959.548532.jpg"

///api/quiz/uploadImage/
//Params: <QueryDict: {}>
//b'--d8d4a663-444f-4895-9f4d-e9eac687d50b\r\nContent-Disposition: form-data; name="ansimage"; filename="BRT101.ZL_786srh@gmail.com_88961575981427.jpg"\r\nContent-Type: image/jpeg\r\nContent-Length: 579477\r\n\r\n\xff\xd8\xff\xe0\x00\x10JFIF\x00\x01\x01\x00\x00\x01\x00\x01\x00\x00\xff\xdb\x00C\x00\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\xff\xdb\x00C\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\xff
