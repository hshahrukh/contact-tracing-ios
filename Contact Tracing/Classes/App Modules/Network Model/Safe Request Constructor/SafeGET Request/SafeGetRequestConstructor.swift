//
//  SafeGETRequestConstructor.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation
import UIKit
import Alamofire



struct SafeGETRequestConstructor {
    
    static func getResponseForRequest(request: SafeRequestModal, callback: @escaping NetworkClosure) {
        
//        if request.operationUrl == "api/course/" {
//            return
//        }
        guard request.baseUrlL != nil && request.operationUrl != nil && request.baseUrlL?.count ?? 0 > 0  && request.operationUrl?.count ?? 0 > 0 else {
            return
        }
            
        let url = request.baseUrlL! + request.operationUrl!
        print(url);
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: request.requestHeader).responseJSON { (response) in
            
            #if DEBUG
                
                print("************************ Request Start**************************");
                print(response.request!)  // original URL request
                print("************************ Request End **************************");
                
                print("************************ Response Data **************************");
                let data = response.data
                var datastring = "";
                if(data != nil){
                    #if DEBUG
                        datastring = String(data: data!, encoding: String.Encoding.utf8)!
                        print(datastring)
                    #endif
                }else{
                    print("\n\n valid response not found");
                }
                
                
                print("************************ Response Results **************************");
                print(response.result)   // result of response serialization
                print("************************ Response Results END **************************");
                
                print("test");
                
            #endif
            
            let operationSuccessful = true
            let message = ""
            var responseObj = SafeNetObject.init(response: response.result.value as AnyObject, status: response.response?.statusCode, err: response.result.error as NSError?, successful: response.result.isSuccess, opSuccessful: operationSuccessful, message: message)
            if request.cachesData == true {
                let data = response.data
                var dataString = ""
                if (data != nil) {
                    dataString = String(data: data!, encoding: String.Encoding.utf8)!
                }
                responseObj.responseString = dataString
            }
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
          //  if GlobalFN().updateApp(responseObj.responseObject , (appDelegate.window!.rootViewController)!) {
             //   return
           // }
            
            callback(responseObj)
        }
    }
}
