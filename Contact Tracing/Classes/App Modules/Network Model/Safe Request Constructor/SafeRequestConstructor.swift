//
//  SafeRequestConstructor.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation
import UIKit

class SafeRequestConstructor {
    
    let urlFilePath: String
    let serviceUrls: NSDictionary
    var baseUrl: String? = nil
    var debugBaseUrl: String? = nil
    var releaseBaseUrl: String? = nil
    var headers: NSDictionary
    
    func processRequestFor(request: SafeRequestModal, callback: @escaping NetworkClosure) {
        
        var request = request
        self.processRequestInfo(requestModal: &request)
        if NwReachability.isConnecteddToNetwork() {
            print("Net Connected !!")
            
            if(request.isLoadStub == true){
//                StubManager.getStubData(request: request, callback: callback)
                
            } else {
                switch request.requestType {
                case .GET:
                    SafeGETRequestConstructor.getResponseForRequest(request: request, callback: { (response) in
                        self.handleNetworkError(netResponse: response,request: request , callback: callback)
                    })
                    break
                case .POST:
                    
                    if(request.isMultiPart){
                        
                            SafePOSTRequestConstructor.getResponseForMultipartRequest(request) { (netResponse:SafeNetObject) -> (Void) in
                                self.handleNetworkError(netResponse: netResponse,request: request , callback: callback);
                    }
                    } else {
                        SafePOSTRequestConstructor.getResponseForRequest(request: request, callback: { (response) in
                            self.handleNetworkError(netResponse: response,request: request , callback: callback)
                        })
                    }
                    break
                case .DOWNLOAD:
                 //   SafeDownloadRequestConstructor.getDownloadResponseForRequest(request: request, callback: { (response) in
                   //     self.handleNetworkError(netResponse: response,request: request , callback: callback)
                    //})
                    break
                case .PUT:
                    break
                case .DELETE:
                    break
                    
                default:
                    break
                    }
                
            }
        }
        else {
            let failedNwObject = SafeNetObject.init(response: nil, status: nil, err: nil, successful: false, opSuccessful: false, message: "No Internet")
            callback(failedNwObject);

            SwiftSpinner.hide()
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
         
//            if(NetworkOperations.kMarkAttendence  == request.urlIdentifier) {
//                
//                
//                let dicData =  SafeSharedData.getUnsubmittedAttendence();
//                var countOfUnsubmittedData = 0;
//                var fileData = ""
//                for (key,value) in dicData {
//                    if let attendanceDic = value as? Dictionary<String, Any> {
//                        if( attendanceDic["userId"] as! String == GlobalFN().userid){
//                            countOfUnsubmittedData+=1;
//                        }
//
//                      
//                    }
//                }
//                
//                if let name =  request.inputParameters!["attendance_image_file"] as? String {
//                    fileData = name
//                }
//         
//                displayAlertWithConfirm(titleStr:"Network Error" , messageStr: "Attendance marking has been saved for submission later.Total pending attendance submission for userId " + GlobalFN().userid + " = " + String(format: "%d", countOfUnsubmittedData) , controller: (appDelegate.window!.rootViewController)! , completion: {
//                    
//                    
//                });
//                
//
//            }
//            
            
            
//             Toast(text: GlobalFN().noInternetError, duration: Delay.long).show()
            
           
    
        }
        
    }
    
    
    func handleNetworkError( netResponse: SafeNetObject, request:SafeRequestModal, callback:NetworkClosure){
        SwiftSpinner.hide()

        if netResponse.statusCode == NetworkResponseConstants.kUnauthorized {
            GlobalFN().token = "";
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if let navigationController = appDelegate.window!.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            navigationController.pushViewController(vc, animated: true);
            return;
            }
        }
        else {
            callback(netResponse);
            
        }
     
    }
    
    
    func showAlertAppDelegate(title : String,message : String,buttonTitle : String,window: UIWindow){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: nil))
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    init(filePath: String) {
        self.urlFilePath = filePath
        self.serviceUrls = NSDictionary.init(contentsOfFile: filePath)!
        let urlDict = self.serviceUrls.object(forKey: "BASE_URLS") as? NSDictionary
        self.baseUrl = urlDict?.object(forKey: "BASE_URL") as? String
        self.debugBaseUrl = urlDict?.object(forKey: "DEBUG_BASE_URL") as? String
        self.releaseBaseUrl = urlDict?.object(forKey: "PROD_BASE_URL") as? String
        self.headers = (self.serviceUrls.object(forKey: "REQUEST_HEADERS") as? NSDictionary)!
    }
    
    
    func readUrls() {
        self.baseUrl = self.serviceUrls.object(forKey: "BASE_URL") as? String
        self.debugBaseUrl = self.serviceUrls.object(forKey: "DEBUG_BASE_URL") as? String
        self.releaseBaseUrl = self.serviceUrls.object(forKey: "PROD_BASE_URL") as? String
        self.headers = (self.serviceUrls.object(forKey: "REQUEST_HEADERS") as? NSDictionary)!
    }
    
    
    func processRequestInfo(requestModal: inout SafeRequestModal) {
        
        let operation = requestModal.urlIdentifier
        let urlKey = "URLS"

        
//        if requestModal.urlIdentifier == NetworkOperations.kUserLogin {
//            let baseUrls = "BASE_URLS"
//            if let passcode = requestModal.inputParameters?["passcode"] as? String , passcode.count > 2 {
//                let info = self.serviceUrls.object(forKey: baseUrls) as? NSDictionary
//                let suffix = String(passcode.characters.suffix(2))
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//                guard let url = info?.object(forKey: suffix) as? String  else {
//                    displayAlert(titleStr: "", messageStr: "Wrong Passcode", controller:   (appDelegate.window!.rootViewController)!, completion: {
////                         return
//                    })
//                    return
//                }
//                GlobalFN().BASE_URL = info?.object(forKey: suffix) as? String ?? info?[0] as! String
//            }
//        }
        
        var baseUrl = GlobalFN().BASE_URL
//        var modalClassName: String = ""
        var opertationUrl: String?
        var requestType: RequestType?
//        var spinnerText: String = ""
        
        let info = self.serviceUrls.object(forKey: urlKey) as? NSDictionary
        let operationInfo = info?.object(forKey: operation!)
        
        if let oInfo = operationInfo {
//            baseUrl = (oInfo as AnyObject).object(forKey: "BASE_URL") as? String
            baseUrl = GlobalFN().BASE_URL
            opertationUrl = (oInfo as AnyObject).object(forKey: "OPERATION_URL") as? String
            
            let request = (oInfo as AnyObject).object(forKey: "REQUEST_TYPE") as? String
            
            if  let qparam = requestModal.inputParameters?["queryParam"] as? String , ( request ==  "GET" || request ==  "DOWNLOAD" ) {
                opertationUrl = opertationUrl! + qparam
                print("")
                
            }
            
            
            if request != nil {
                requestType = RequestType(rawValue: request!)
            }
        }

        var headers = self.serviceUrls.object(forKey: "REQUEST_HEADERS") as? [String:String];
        if(requestModal.tokenRequired){
//             'Cache-Control':'no-cache'
            if let authorizationKey = requestModal.getAuthorizationHeader(){

                headers?.updateValue(authorizationKey, forKey: "access-token")
            }
        }
        
        if requestModal.images != nil {
            requestModal.isMultiPart = true
        }
        
 //       headers?.updateValue( GlobalFN().appVersion, forKey: "safeVersion")
 //       headers?.updateValue( "", forKey: "deviceModel")
 //       headers?.updateValue( "", forKey: "osVersion")
        if(operation == "Logout"){
            print("adding header !")
            headers?.updateValue(GlobalFN().token, forKey: "xs-token")
        }
       
//        if GlobalFN().crtfToken.count > 1 {
//        headers?.updateValue( GlobalFN().crtfToken, forKey: "X-CSRFToken")
//        }
        
        var relevantUrl = debugBaseUrl
        
        #if DEBUG
            relevantUrl = debugBaseUrl
        #else
            relevantUrl = releaseBaseUrl
        #endif
        
        if (baseUrl.count == 0) {
            if (relevantUrl!.count == 0) {
                baseUrl = self.baseUrl!
            } else {
                baseUrl = relevantUrl!
            }
        }
        requestModal.requestHeader = headers
        requestModal.baseUrlL = baseUrl as String
        if (requestType != nil) {
            requestModal.requestType = requestType!
        }
        
        if let isTrue = requestModal.urlParameters {
            if isTrue.count > 0 {
                var inputUrl = opertationUrl!
                for (key, value) in requestModal.urlParameters! {
                    let resultString = inputUrl.replacingOccurrences(of: key, with: value)
                    inputUrl = resultString
                }
                opertationUrl = inputUrl
            }
        } else {
            requestModal.operationUrl = opertationUrl!
        }
        requestModal.operationUrl = opertationUrl!
    }
}
