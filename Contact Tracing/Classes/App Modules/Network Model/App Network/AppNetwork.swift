//
//  AppNetwork.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation
import UIKit


class AppNetwork: NSObject {
    
    static let sharedInstance = AppNetwork()
    var requestConstructor: SafeRequestConstructor?
    var timer: Timer?
    var alertShown: Bool?
    
    override init() {
        let networkFilePath = Bundle.main.path(forResource: "AppNetwork", ofType: "plist")
        requestConstructor = SafeRequestConstructor.init(filePath: networkFilePath!)
    }
    
    func getNetworkResponseFor(request: SafeRequestModal, callback: @escaping NetworkClosure) {
//        if AppReachability.isNetworkReachable == true {
            requestConstructor?.processRequestFor(request: request, callback: callback)
            self.closeTimer()
//        }
//        else {
//            SwiftSpinner.hide()
//             Toast(text: "Please check your internet connection", duration: Delay.short).show()
//            
//            
//        }
//        else if (alertShown == false) {
//            let appDelegate = UIApplication.shared.delegate
//            self.alertShown = true
//            self.startTimer()
//        }
    }
    
    func startTimer() {
        closeTimer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateAlertStatus), userInfo: nil, repeats: false)
    }
    
    func closeTimer() {
        if self.timer != nil {
            timer?.invalidate()
        }
    }
    
    @objc func updateAlertStatus() {
        self.alertShown = false
    }
    
    func showAlertAppDelegate(title : String,message : String,buttonTitle : String,window: UIWindow){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: nil))
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
