//
//  AppReachability.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//
import Foundation
import Alamofire

class AppReachability: NSObject {
    
    static let sharedInstance = AppReachability()
    let manager = NetworkReachabilityManager(host: "www.google.com")
    var isReachable: Bool = true
    static var isNetworkReachable: Bool = true
    
    override init() {
        super.init()
        self.startNetworkReachabilityObserver()
        self.isReachable = self.manager!.isReachable
        print("network reachable \(self.manager!.isReachable)")
        AppReachability.isNetworkReachable = self.isReachable
    }

    func startNetworkReachabilityObserver() {
        
        manager?.listener = { status in
            
            switch status {
            case .notReachable:
                print("not reachable")
                break
            case .reachable(.wwan):
                print("wwan")
                break
            case .reachable(.ethernetOrWiFi):
                print("ethernet or WiFi")
                break
            case .unknown:
                print("unknown")
                break
            }
            
            print("Network status changed: \(status)")
            print("Network reachble \(self.manager!.isReachable)")
            self.isReachable = self.manager!.isReachable
            AppReachability.isNetworkReachable = self.manager!.isReachable
            
        }
        manager?.startListening()
        
    }
    
}


