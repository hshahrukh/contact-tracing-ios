//
//  SafeNetworkConstants.swift
 
//
//  Created by BodhiTree on 09/08/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation

struct NetworkOperations {
    
//    Add Constants Here
    static let kUserLogin = "UserLogin" ;
    static let kGenerateOTP="GenerateOTP"
    static let kVerifyOTP="VerifyOTP"
    static let kLogout="Logout"
    static let kUploadData="UploadData"
}




struct NetworkResponseConstants {
    
    static let kSuccess = 200
    static let kInternalServerError = 500
    static let kNotFound = 404
    static let kUnauthorized = 401
    static let kBadRequest = 400

}

typealias NetworkClosure = (_ netResponse: SafeNetObject)-> Void

enum RequestType: String {
    
    case NONE
    case GET
    case POST
    case PUT
    case DELETE
    case DOWNLOAD

}
