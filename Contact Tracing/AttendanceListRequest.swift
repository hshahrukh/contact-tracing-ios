//
//  AttendanceListRequest.swift
//  QuizApp
//
//  Created by BodhiTree on 26/11/18.
//  Copyright © 2018 Bhaskaran Raman. All rights reserved.
//

import Foundation


struct PastAttendanceRequest:Codable {
    var start_date:String
    var course:String
    var end_date:String
    
    func toDict()-> Dictionary<String, Any> {
        
        
        do {
         let  dic = try JSONDecoder().decode([String: String].self, from: JSONEncoder().encode(self))
        return dic
        }
        catch{
            print("error")
        }
        return Dictionary <String,Any>()
  
    }
    
}


struct PastAttendanceListResponse:Codable {
    var attendance_array:Array<AttendanceRecordFields>
}

struct AttendanceRecordFields:Codable {
    var date:String
    var failure_reasons:Array<String>
    var time:String
    var state:String
}



struct otpRequest:Codable {
    var phone:String
    var device_id:String
    var app_id:String

    func toDict()-> Dictionary<String, Any> {
        
        
        do {
            let  dic = try JSONDecoder().decode([String: String].self, from: JSONEncoder().encode(self))
            return dic
        }
        catch{
            print("error")
        }
        return Dictionary <String,Any>()
        
    }
    
}

struct otpVerifyRequest:Codable {
    var phone:String
    var device_id:String
    var app_id:String
    var otp:String

    func toDict()-> Dictionary<String, Any> {
        
        
        do {
            let  dic = try JSONDecoder().decode([String: String].self, from: JSONEncoder().encode(self))
            return dic
        }
        catch{
            print("error")
        }
        return Dictionary <String,Any>()
        
    }
    
}

struct gooleLoginRequest:Codable {
    var token_id:String?
    var email_id:String
    var passcode:String
    
    func toDict()-> Dictionary<String, Any> {
        
        
        do {
            let  dic = try JSONDecoder().decode([String: String].self, from: JSONEncoder().encode(self))
            return dic
        }
        catch{
            print("error")
        }
        return Dictionary <String,Any>()
        
    }
    
}


//let parameters : [String : String] = [
//    "token_id" : self.googleToken ?? "",
//    "email_id" : self.ldapTextField.text ?? "",
//    "passcode" : self.passwordTextField.text ?? ""
//]
