//
//  GlobalFn.swift
//  Contact Tracing
//
//  Created by Synerg IITBombay on 01/04/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import Foundation
import KeychainAccess

open class GlobalFN: NSObject {
    let OTP_LENGTH = 6
    let OTP_MIN_INTERVAL = 30
    let statusCodeNot200 =  "Something went wrong."
    let UniqueDeviceKey = "UUID_apricot-1"
    let unreachable =  "Network is unreachable."
    let app_id = "200001"
    var otp = "0000000"
    var appVersion:String {
        get {
            return "i0.0.2"
       }
    }
    
    static let buildDate:String =   {
            return "5 Apr, 2020"
        }()
    
    var phoneNum: String {
        get {
            if let returnValue = UserDefaults.standard.object(forKey: "phoneNum") as? String {
                return returnValue
            } else {
                return "" //Default value
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "phoneNum")
            UserDefaults.standard.synchronize()
        }
    }
    
    var token: String {
        get {
            if let returnValue = UserDefaults.standard.object(forKey: "token") as? String {
                return returnValue
            } else {
                return "" //Default value
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "token")
            UserDefaults.standard.synchronize()
        }
    }
    
    var user_id: String {
        get {
            if let returnValue = UserDefaults.standard.object(forKey: "user_id") as? String {
                return returnValue
            } else {
                return "" //Default value
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "user_id")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var lastTimestampOfCreatingBinFile: Int64 {
        get {
            if let returnValue = UserDefaults.standard.object(forKey: "lastTimestampOfCreatingBinFile") as? Int64 {
                return returnValue
            } else {
                return -1 //Default value
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "lastTimestampOfCreatingBinFile")
            UserDefaults.standard.synchronize()
        }
    }
    
    var pseudo_id_list: NSArray{
        get {
            if let returnValue = UserDefaults.standard.object(forKey: "pseudo_id_list") as? NSArray{
                return returnValue
            } else {
                return [] //Default value
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "pseudo_id_list")
            UserDefaults.standard.synchronize()
        }
    }


    
    
    var OTP: String {
        get {
            return otp
        }
        set {
            otp = newValue
        }
    }
    
    var keychainUniqueDeviceId:String {
          get {
              let bundleName = Bundle.main.bundleIdentifier
              let keychain = Keychain(service: bundleName ?? "apricot")
              if let id = keychain[UniqueDeviceKey]  {
                  return id
              }
              return ""
          }
          set {
              let bundleName = Bundle.main.bundleIdentifier
              let keychain = Keychain(service: bundleName ?? "apricot")

              if let t = keychain[UniqueDeviceKey]  {
                  print("key already saved : "+t)
              }
              else {
                  keychain[UniqueDeviceKey] = newValue

              }
              
          }
      }
    func jsonToDict(jsonString: String) -> NSDictionary {
           let jsonData = jsonString.data(using: String.Encoding.utf8)
           let dataDict = try!  JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)
           return dataDict as! NSDictionary
       }
    func dictToJSON(dict: NSDictionary) -> String {
           
           let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
           let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
           return jsonString
       }
    
        var BASE_URL: String {
            get {
                if let returnValue = UserDefaults.standard.object(forKey: "BASE_URL") as? String {
                   // return "http://10.129.131.185:8000/"
                    return returnValue
                } else {
                      return "https://apricot.safe-analytics.in/"
                }
            }
            set {
                UserDefaults.standard.set(newValue, forKey: "BASE_URL")
                UserDefaults.standard.synchronize()
            }
        }
        
        
    func  setBASE_URL(initial: String) -> Bool{
        let url = "https://apricot.safe-analytics.in/"
        BASE_URL = url
        return true
        
    }
    


}
