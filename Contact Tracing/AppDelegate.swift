//
//  AppDelegate.swift
//  Contact Tracing
//
//  Created by ShahRukh on 31/03/20.
//  Copyright © 2020 iitb. All rights reserved.
//

import UIKit
//import BeaconService
import Firebase

import FirebaseCrashlytics

@UIApplicationMain




class AppDelegate: UIResponder, UIApplicationDelegate  {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
//        Fabric.with([Crashlytics.self])
//
//        Crashlytics.sharedInstance().crash()
//        Fabric.sharedSDK().debug = true

//        fatalError()

        
        //set uuid if not already set
        if(GlobalFN().keychainUniqueDeviceId == ""){
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                GlobalFN().keychainUniqueDeviceId = uuid;
           }
        }
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {

        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        debugPrint("Notif")
        UIPasteboard.general.string=""

    }
    
  
}


extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}

